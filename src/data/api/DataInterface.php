<?php
/**
 * Description :
 * This class allows to describe behavior of data class.
 * Data allows to manage values from a specified source of data.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\data\data\api;



interface DataInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified value exists.
     *
     * @param mixed $key
     * @return boolean
     */
    public function checkValueExists($key);





	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get data source.
	 * 
	 * @return mixed
	 */
	public function getDataSrc();



    /**
     * Get index array of keys.
     *
     * @return array
     */
    public function getTabKey();



    /**
     * Get associative array of values.
     * Format: key => value.
     *
     * @return array
     */
    public function getTabValue();



    /**
     * Get a specified value.
     *
     * @param mixed $key
     * @return null|mixed
     */
    public function getValue($key);
	
	
	
	
	
	// Methods setters
	// ******************************************************************************

	/**
	 * Set data source.
	 * 
	 * @param mixed $dataSrc
	 */
	public function setDataSrc($dataSrc);
	
	

	/**
     * Set (update) a specified value.
	 * 
	 * @param mixed $key
	 * @param mixed $value
	 * @return boolean: true if correctly set, false else
	 */
	public function setValue($key, $value);

	
	
	/**
     * Add a specified value.
	 * 
	 * @param mixed $key
	 * @param mixed $value
	 * @return boolean: true if correctly added, false else
	 */
	public function addValue($key, $value);
	
	
	
	/**
     * Put a specified value (update if exists, add else).
	 * 
	 * @param mixed $key
	 * @param mixed $value
	 * @return boolean: true if correctly put, false else
	 */
	public function putValue($key, $value);
	
	
	
	/**
	 * Remove a specified value.
	 * 
	 * @param mixed $key
	 * @return boolean: true if correctly removed, false else
	 */
	public function removeValue($key);



    /**
     * Remove all values.
     *
     * @return boolean: true if correctly removed, false else
     */
    public function removeValueAll();
}