<?php
/**
 * Description :
 * This class allows to define handle data class.
 * Handle data allows to handle data control by following features:
 * -> Check parameter values.
 * -> Set events on parameter values actions (Add, set (update) and remove).
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\data\data\handle\model;

use liberty_code\data\data\model\DefaultData;

use liberty_code\data\data\handle\exception\HandleKeyInvalidFormatException;
use liberty_code\data\data\handle\exception\HandleValueInvalidFormatException;
use liberty_code\data\data\handle\exception\HandleKeyNotRemoveException;



abstract class HandleData extends DefaultData
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods validation
	// ******************************************************************************

	/**
	 * Check if specified key is valid.
	 * Overwrite it to set specific rules.
	 *
	 * @param mixed $key
	 * @param string|\Exception &$error = null
	 * @return boolean
	 */
	public function checkValidKey($key, &$error = null)
	{
		// Return result
		return true;
	}



	/**
	 * Check if specified value is valid, for specified key.
	 * Overwrite it to set specific rules.
	 *
	 * @param mixed $key
	 * @param mixed $value
	 * @param string|\Exception &$error = null
	 * @return boolean
	 */
	public function checkValidValue($key, $value, &$error = null)
	{
		// Return result
		return true;
	}



	/**
	 * Check if value is removable, for specified key.
	 * Overwrite it to set specific rules.
	 *
	 * @param mixed $key
	 * @param string|\Exception &$error = null
	 * @return boolean
	 */
	public function checkValidRemove($key, &$error = null)
	{
		// Return result
		return true;
	}



	/**
	 * Set validation for specified key.
	 *
	 * @param mixed $key
	 * @throws \Exception
	 */
	protected function setValidKey($key)
	{
		// Init var
		$error = null;

		// Throw exception if validation not pass
		if(!$this->checkValidKey($key, $error))
		{
			// Throw custom exception
			if($error instanceof \Exception)
			{
				throw $error;
			}
			// Throw standard exception
			else
			{
				throw new HandleKeyInvalidFormatException($key, $error);
			}
		}
	}



	/**
	 * Set validation for specified value and specified key.
	 *
	 * @param mixed $key
	 * @param mixed $value
	 * @throws \Exception
	 */
	protected function setValidValue($key, $value)
	{
		// Init var
		$error = null;

		// Throw exception if validation not pass
		if(!$this->checkValidValue($key, $value, $error))
		{
			// Throw custom exception
			if($error instanceof \Exception)
			{
				throw $error;
			}
			// Throw standard exception
			else
			{
				throw new HandleValueInvalidFormatException($key, $value, $error);
			}
		}
	}



	/**
	 * Set validation that specified key is removable.
	 *
	 * @param mixed $key
	 * @throws \Exception
	 */
	protected function setValidRemove($key)
	{
		// Init var
		$error = null;

		// Throw exception if validation not pass
		if(!$this->checkValidRemove($key, $error))
		{
			// Throw custom exception
			if($error instanceof \Exception)
			{
				throw $error;
			}
			// Throw standard exception
			else
			{
				throw new HandleKeyNotRemoveException($key, $error);
			}
		}
	}
	
	


	
	// Methods events
	// ******************************************************************************
	
	/**
	 * Event before adding specified value to specified key.
	 * Overwrite it to set specific feature.
	 *
	 * @param mixed $key
	 * @param mixed $value
	 */
	protected function onBeforeAddValue($key, $value)
	{
		// Do nothing
	}



	/**
	 * Event after adding value to specified key.
	 * Overwrite it to set specific feature.
	 *
	 * @param mixed $key
	 */
	protected function onAfterAddValue($key)
	{
		// Do nothing
	}



	/**
	 * Event before setting specified value to specified key.
	 * Overwrite it to set specific feature.
	 *
	 * @param mixed $key
	 * @param mixed $value
	 */
	protected function onBeforeSetValue($key, $value)
	{
		// Do nothing
	}



	/**
	 * Event after setting value to specified key.
	 * Overwrite it to set specific feature.
	 *
	 * @param mixed $key
	 */
	protected function onAfterSetValue($key)
	{
		// Do nothing
	}



	/**
	 * Event before removing specified key.
	 * Overwrite it to set specific feature.
	 *
	 * @param mixed $key
	 */
	protected function onBeforeRemoveValue($key)
	{
		// Do nothing
	}



	/**
	 * Event after removing specified key.
	 * Overwrite it to set specific feature.
	 *
	 * @param mixed $key
	 */
	protected function onAfterRemoveValue($key)
	{
		// Do nothing
	}





	// Methods getters
	// ******************************************************************************

    /**
     * Get a specified value engine.
     * Overwrite it to set specific feature.
     *
     * @param string|integer $key
     * @return null|mixed
     */
    abstract protected function getValueEngine($key);



	/**
	 * @inheritdoc
     * @throws HandleKeyInvalidFormatException
	 */
	public function getValue($key)
	{
		// Set validation
		$this->setValidKey($key);

		// Return parent method
		return $this->getValueEngine($key);
	}





	// Methods setters
	// ******************************************************************************

	/**
	 * @inheritdoc
     * @throws HandleKeyInvalidFormatException
     * @throws HandleValueInvalidFormatException
	 */
	public function setValue($key, $value)
	{
		// Set validation
		$this->setValidKey($key);
		$this->setValidValue($key, $value);

		// Event before
		$this->onBeforeSetValue($key, $value);

		// Call parent method: set value
		$result = parent::setValue($key, $value);

		// Event after
		$this->onAfterSetValue($key);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 * @throws HandleKeyInvalidFormatException
     * @throws HandleValueInvalidFormatException
	 */
	public function addValue($key, $value)
	{
		// Set validation
		$this->setValidKey($key);
		$this->setValidValue($key, $value);

		// Event before
		$this->onBeforeAddValue($key, $value);

		// Call parent method: add value
		$result = parent::addValue($key, $value);

		// Event after
		$this->onAfterAddValue($key);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
     * @throws HandleKeyInvalidFormatException
     * @throws HandleKeyNotRemoveException
	 */
	public function removeValue($key)
	{
		// Set validation
		$this->setValidKey($key);
		$this->setValidRemove($key);

		// Event before
		$this->onBeforeRemoveValue($key);

		// Call parent method: remove value
		$result = parent::removeValue($key);

		// Event after
		$this->onAfterRemoveValue($key);

		// Return result
		return $result;
	}



}