<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\data\data\handle\library;



class ConstHandleData
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	// Exception message constants
	const EXCEPT_MSG_HANDLE_KEY_INVALID_FORMAT = 'Following key "%1$s" failed validation handle rule!%2$s';
	const EXCEPT_MSG_HANDLE_VALUE_INVALID_FORMAT = 'Following value "%2$s" (for path "%1$s") failed validation handle rule!%3$s';
	const EXCEPT_MSG_HANDLE_KEY_NOT_REMOVE = 'Following key "%1$s" failed validation handle rule to remove it!%2$s';
}