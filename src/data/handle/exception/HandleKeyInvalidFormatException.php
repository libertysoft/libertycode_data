<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\data\data\handle\exception;

use liberty_code\data\data\handle\library\ConstHandleData;



class HandleKeyInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $key
	 * @param mixed $errorMsg
     */
	public function __construct($key, $errorMsg)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$strErrorMsg =
			(
				(!is_null($errorMsg)) &&
				is_string($errorMsg) && (trim($errorMsg) != '')
			) ?
				' ' . trim($errorMsg) :
				'';
		$this->message = sprintf(ConstHandleData::EXCEPT_MSG_HANDLE_KEY_INVALID_FORMAT, strval($key), $strErrorMsg);
	}
	
	
	
}