<?php
/**
 * Description :
 * This class allows to define default data.
 * Can be consider is base of all data type.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\data\data\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\data\data\api\DataInterface;

use liberty_code\data\data\library\ConstData;
use liberty_code\data\data\exception\KeyFoundException;
use liberty_code\data\data\exception\KeyNotFoundException;



abstract class DefaultData extends FixBean implements DataInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param mixed $dataSrc = null
     */
    public function __construct($dataSrc = null)
    {
        // Call parent constructor
        parent::__construct();

        // Hydrate data source, if required
        if(!is_null($dataSrc))
        {
            $this->setDataSrc($dataSrc);
        }
    }





	// Methods initialize
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	protected function beanHydrateDefault()
	{
		// Init bean data
		if(!$this->beanExists(ConstData::DATA_KEY_DEFAULT_DATA_SRC))
		{
            $this->__beanTabData[ConstData::DATA_KEY_DEFAULT_DATA_SRC] = null;
		}
	}
	
	
	
	
	
	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
			ConstData::DATA_KEY_DEFAULT_DATA_SRC
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkValueExists($key)
    {
        return (!is_null($this->getValue($key)));
    }





	// Methods getters
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function getDataSrc()
	{
		// Return result
		return $this->beanGet(ConstData::DATA_KEY_DEFAULT_DATA_SRC);
	}



    /**
     * @inheritdoc
     */
    public function getTabValue()
    {
        // Init var
        $result = array();
        $tabKey = $this->getTabKey();

        // Run all keys
        foreach($tabKey as $key)
        {
            // Register item
            $result[$key] = $this->getValue($key);
        }

        // Return result
        return $result;
    }
	



	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function setDataSrc($dataSrc)
	{
		$this->beanSet(ConstData::DATA_KEY_DEFAULT_DATA_SRC, $dataSrc);
	}



    /**
     * Set (update) a specified value engine.
     * Overwrite it to set specific feature.
     *
     * @param mixed $key
     * @param mixed $value
     * @return boolean: true if correctly set, false else
     * @throws KeyNotFoundException
     */
    protected function setValueEngine($key, $value)
    {
        // Remove
        $result = $this->removeValue($key);

        // Add
        $result = $this->addValue($key, $value) && $result;

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function setValue($key, $value)
    {
        // Check if value not exists for key: throw exception
        if(!$this->checkValueExists($key))
        {
            throw new KeyNotFoundException($key);
        }

        // Return result
        return $this->setValueEngine($key, $value);
    }



    /**
     * Add a specified value engine.
     * Overwrite it to set specific feature.
     *
     * @param mixed $key
     * @param mixed $value
     * @return boolean: true if correctly added, false else
     */
    abstract protected function addValueEngine($key, $value);



    /**
     * @inheritdoc
     * @throws KeyFoundException
     */
    public function addValue($key, $value)
    {
        // Check if value exists for key: throw exception
        if($this->checkValueExists($key))
        {
            throw new KeyFoundException($key);
        }

        // Return result
        return $this->addValueEngine($key, $value);
    }



	/**
	 * @inheritdoc
	 */
	public function putValue($key, $value)
	{
		// Check value exists for path: update
		if($this->checkValueExists($key))
		{
			$result = $this->setValue($key, $value);
		}
		// Else: add
		else
		{
			$result = $this->addValue($key, $value);
		}
		
		// Return result
		return $result;
	}



    /**
     * Remove a specified value engine.
     * Overwrite it to set specific feature.
     *
     * @param mixed $key
     * @return boolean: true if correctly removed, false else
     */
    abstract protected function removeValueEngine($key);



    /**
     * @inheritdoc
     * @throws KeyNotFoundException
     */
    public function removeValue($key)
    {
        // Check if value not exists for key: throw exception
        if(!$this->checkValueExists($key))
        {
            throw new KeyNotFoundException($key);
        }

        // Return result
        return $this->removeValueEngine($key);
    }



    /**
     * @inheritdoc
     */
    public function removeValueAll()
    {
        // Ini var
        $result = true;
        $tabKey = $this->getTabKey();

        // Run all items
        foreach($tabKey as $key)
        {
            // Remove item
            $result = $this->removeValue($key) && $result;
        }

        // Return result
        return $result;
    }



}