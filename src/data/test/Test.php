<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/data/test/PathTableDataTest.php');

// Use
use liberty_code\data\data\test\PathTableDataTest;



// Init var
$tabDataSrc = array(
	'k1' => 1,
	'k2' => [
		'k2_1' => 2.1,
		'k2_2' => [
			'k2_2_1' => true,
			'k2_2_2' => 'Value 2_2_2',
			'k2_2_3' => [
				'k2_2_3_1' => 'Value 2_2_3_1',
				'k2_2_3_2' => 'Value 2_2_3_2'
			]
		]
	],
	'k3' => [
		'k3_1' => 'Value 3_1',
		'k3_2' => 'Value 3_2'
	],
	'k4' => false
);

$objData = new PathTableDataTest();



// Test properties
echo('Test properties : <br />');

$objData->setDataSrc($tabDataSrc);
echo('Source data: <pre>');print_r($objData->getDataSrc());echo('</pre>');
echo('Config: <pre>');print_r($objData->getTabConfig());echo('</pre>');
echo('Path separator: <pre>');print_r($objData->getStrConfigPathSeparator());echo('</pre>');

$tabConfig = array(
    'path_separator' => ':'
);
$objData->setTabConfig($tabConfig);
echo('Config: <pre>');print_r($objData->getTabConfig());echo('</pre>');
echo('Path separator: <pre>');print_r($objData->getStrConfigPathSeparator());echo('</pre>');

$tabConfig = array(
    'path_separator' => '/'
);
$objData->setTabConfig($tabConfig);
echo('Config: <pre>');print_r($objData->getTabConfig());echo('</pre>');
echo('Path separator: <pre>');print_r($objData->getStrConfigPathSeparator());echo('</pre>');

echo('<br /><br /><br />');



// Test check, get
$tabPath = array(
	'k1', // Ok: Found
	'k2/k2_2', // Ok: Found
	'k2/k2_2/k2_2_3', // Ok: Found
	'k3', // Ok: Found
	'k4', // Ok: Found
	'test', // Ko: not found
	'/', // Ko: Bad path format test
	'', // Ko: Bad path format test
	'test//test2', // Ko: Bad path format test
	'k2/k2_2/', // Ko: Bad path format test
	'k7/k7_1' // Ko: Bad path format (handle rule) test
);

foreach($tabPath as $strPath)
{
	echo('Test check, get "'.$strPath.'": <br />');
	try{
		echo('Check exists: <pre>');var_dump($objData->checkValueExists($strPath));echo('</pre>');
		echo('Get: <pre>');var_dump($objData->getValue($strPath));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('<br /><br /><br />');



// Test set
$tabPath = array(
	'k1' => 1.1, // Ok: Found
	'k2/k2_2/k2_2_1' => true, // Ok: Found
	'k2/k2_2/k2_2_3' => 'Value 2_2_3', // Ok: Found
	'k1/k1_1' => 'Value 1_1', // Ko: Not found
	'k2/k2_2' => null, // Ko: Bad value format test
	'k2/k2_2/' => 'Value 2_2_3', // Ko: Bad path format test
    'k3/k3_1/k3_1_1/0' => 'Value 3_1_1 test', // Ko: Not found
	'k7/k7_1' => 'Value 7_1' // Ko: Bad path format (handle rule) test
);

foreach($tabPath as $strPath => $value)
{
	echo('Test set "'.$strPath.'": <br />');
	try{
		echo('<pre>');var_dump($objData->setValue($strPath, $value));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test setting: <br />');
echo('<pre>');print_r($objData->getDataSrc());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test add
$tabPath = array(
	'k1' => 1.2, // Ko: Found
	'k2/k2_2/k2_2_1' => true, // Ko: Found
	'k2/k2_2/k2_2_3/k2_2_3_1' => 'Value 2_2_3_1 updated', // Ok: Create
	'k1/k1_1' => 'Value 1_1', // Ok: Create
	'k2/k2_2/k2_2_3/k2_2_3_2' => null, // Ko: Bad value format test
	'k2/k2_2/' => 'Value 2_2_3', // Ko: Bad path format test
	'k7/k7_1' => 'Value 7_1', // Ko: Bad path format (handle rule) test
	'k2/k2_3' => 'Value 7_2' // Ko: Bad value format (handle rule) test
);

foreach($tabPath as $strPath => $value)
{
	echo('Test add "'.$strPath.'": <br />');
	try{
		echo('<pre>');var_dump($objData->addValue($strPath, $value));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test adding: <br />');
echo('<pre>');print_r($objData->getDataSrc());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test put
$tabPath = array(
	'k1/0' => 1.2, // Ok: Update
	'k1/k1_2' => 1.12, // Ok: Create
	'k2/k2_2/k2_2_1' => false, // Ok: Update
	'k2/k2_2/k2_2_3/k2_2_3_1' => 'Value 2_2_3_1 updated 2', // Ok: Update
	'k2/k2_2/k2_2_3/k2_2_3_2' => 3, // Ok: Create
	'k4/k4_1/k4_1_1/k4_1_1_2' => 'Value 4_1_1_2', // Ok: Create
	'k5' => 'Value 5', // Ok: Create
	'k2/k2_2/k2_2_3/k2_2_3_3' => null, // Ko: Bad value format test
	'k2/k2_2/' => 'Value 2_2_3', // Ko: Bad path format test
	'k7/k7_1' => 'Value 7_1', // Ko: Bad path format (handle rule) test
	'k2/k2_3' => 'Value 7_2', // Ko: Bad value format (handle rule) test
	'k2/k2_4' => 7 // Ok: Create
);

foreach($tabPath as $strPath => $value)
{
	echo('Test put "'.$strPath.'": <br />');
	try{
		echo('<pre>');var_dump($objData->putValue($strPath, $value));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test putting: <br />');
echo('<pre>');print_r($objData->getDataSrc());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test remove
$tabPath = array(
	'k1/0', // Ok: Found
	'k1/0', // Ko: Not found
	'k2/k2_2/k2_2_3', // Ok: Found
	'k2/k2_2/k2_2_3/k2_2_3_1', // Ko: Not found
	'k3/k3_1', // Ok: Found
	'k3/k3_2', // Ok: Found
	'k2/k2_2/k2_2_3/k2_2_3_3', // Ko: Not found
	'k2/k2_2/', // Ko: Bad path format test
	'k7/k7_1', // Ko: Bad path format (handle rule) test
	'k2/k2_3' // Ko: Forbidden (handle rule) test
);

foreach($tabPath as $strPath)
{
	echo('Test remove "'.$strPath.'": <br />');
	try{
		echo('<pre>');var_dump($objData->removeValue($strPath));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test removing: <br />');
echo('<pre>');print_r($objData->getDataSrc());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');


