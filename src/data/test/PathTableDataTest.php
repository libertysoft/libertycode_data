<?php

namespace liberty_code\data\data\test;

use liberty_code\data\data\table\path\model\PathTableData;



class PathTableDataTest extends PathTableData
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
    public function checkValidKey($key, &$error = null)
	{
		// Init var
        $error = null;
		$result = (preg_match('#^k7\/.*#', $key) !== 1); // Must be different that k7/... (ex: k7/k7_1)
		
		if(!$result)
		{
            $error = 'Impossible to use path starting by "k7/...".';
		}
		
		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function checkValidValue($key, $value, &$error = null)
	{
		// Init var
        $error = null;
		$result = 
			($key != 'k2/k2_3') ||
			(($key == 'k2/k2_3') && (is_int($value)));
		
		if(!$result)
		{
            $error = 'The value must be an integer.';
		}
		
		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function checkValidRemove($key, &$error = null)
	{
		// Init var
        $error = null;
		$result = ($key != 'k2/k2_3');

		if(!$result)
		{
            $error = 'Impossible to remove path "' . $key . '".';
		}
		
		// Return result
		return $result;
	}
	
	
	
	
	
	// Methods events
	// ******************************************************************************

    /**
     * Print specified event.
     *
     * @param string $strEvent
     * @param string $key
     * @param mixed $value = null
     */
	protected function printEvent($strEvent, $key, $value = null)
	{
	    if(
	        is_string($value) ||
            is_numeric($value) ||
            is_bool($value)
        )
        {
            var_dump(sprintf(
                'Test event "%1$s" for key "%2$s", value "%3$s".',
                $strEvent,
                $key,
                strval($value)
            ));
        }
	    else
        {
            var_dump(sprintf(
                'Test event "%1$s" for key "%2$s".',
                $strEvent,
                $key
            ));
        }
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	protected function onBeforeAddValue($key, $value)
	{
		$this->printEvent('Before add', $key, $value);
	}



	/**
	 * @inheritdoc
	 */
	protected function onAfterAddValue($key)
	{
		$this->printEvent('After add', $key);
	}



	/**
	 * @inheritdoc
	 */
	protected function onBeforeSetValue($key, $value)
	{
		$this->printEvent('Before set', $key, $value);
	}



	/**
	 * @inheritdoc
	 */
	protected function onAfterSetValue($key)
	{
		$this->printEvent('After set', $key);
	}



	/**
	 * @inheritdoc
	 */
	protected function onBeforeRemoveValue($key)
	{
		$this->printEvent('Before remove', $key);
	}



	/**
	 * @inheritdoc
	 */
	protected function onAfterRemoveValue($key)
	{
		$this->printEvent('After remove', $key);
	}
	
	
	
}