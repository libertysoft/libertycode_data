<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\data\data\library;



class ConstData
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	// Dependency data constants
	const DATA_KEY_DEFAULT_DATA_SRC = 'dataSrc';



	// Exception message constants
	const EXCEPT_MSG_KEY_FOUND = 'Following key "%1s" already exists!';
	const EXCEPT_MSG_KEY_NOT_FOUND = 'Following key "%1s" not found!';
}