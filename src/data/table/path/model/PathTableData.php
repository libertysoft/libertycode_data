<?php
/**
 * Description :
 * This class allows to define path table data.
 * Path table data is table data, where keys are considered as paths (@see ToolBoxPathTableData ).
 *
 * Path table data uses the following specified configuration:
 * [
 *     path_separator(optional: got '/', if not found):
 *         'String path separator'
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\data\data\table\path\model;

use liberty_code\data\data\table\model\TableData;

use liberty_code\data\data\table\path\library\ConstPathTableData;
use liberty_code\data\data\table\path\library\ToolBoxPathTableData;
use liberty_code\data\data\table\path\exception\ConfigInvalidFormatException;
use liberty_code\data\data\table\path\exception\PathInvalidFormatException;



/**
 * @method array getTabConfig() Get config array.
 * @method void setTabConfig(array $tabConfig) Set config array.
 */
class PathTableData extends TableData
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     */
    public function __construct(
        array $tabConfig = null,
        $dataSrc = null
    )
    {
        // Call parent constructor
        parent::__construct($dataSrc);

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }
    }





	// Methods initialize
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	protected function beanHydrateDefault()
	{
		// Init bean data
		if(!$this->beanExists(ConstPathTableData::DATA_KEY_DEFAULT_CONFIG))
		{
            $this->__beanTabData[ConstPathTableData::DATA_KEY_DEFAULT_CONFIG] = array();
		}
		
		// Call parent method
        parent::beanHydrateDefault();
	}
	
	
	
	
	
	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
        // Init var
        $tabKey = array(
            ConstPathTableData::DATA_KEY_DEFAULT_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

		// Return result
		return $result;
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstPathTableData::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
					break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}
	




    // Methods getters
    // ******************************************************************************

    /**
     * Get path separator, from configuration.
     *
     * @return string
     */
    public function getStrConfigPathSeparator()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = ConstPathTableData::CONFIG_DEFAULT_PATH_SEPARATOR;

        // Get path separator, if found
        if(array_key_exists(ConstPathTableData::TAB_CONFIG_KEY_PATH_SEPARATOR, $tabConfig))
        {
            $result = $tabConfig[ConstPathTableData::TAB_CONFIG_KEY_PATH_SEPARATOR];
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @param string $strPath (=> $key)
     */
    protected function getValueEngine($strPath)
    {
        // Return result
        return ToolBoxPathTableData::getValue(
            $this->getUseTabDataSrc(),
            $strPath,
            $this->getStrConfigPathSeparator()
        );
    }



    /**
     * @inheritdoc
     * @param string $strPath (=> $key)
     * @throws PathInvalidFormatException
     */
    public function getValue($strPath)
    {
        // Check path format
        PathInvalidFormatException::setCheck($strPath, $this->getStrConfigPathSeparator());

        // Return result
        return parent::getValue($strPath);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param string $strPath (=> $key)
     */
    protected function setValueEngine($strPath, $value)
    {
        // Return result
        return ToolBoxPathTableData::setValue(
            $this->getUseTabDataSrc(),
            $strPath,
            $value,
            $this->getStrConfigPathSeparator(),
            false
        );
    }



    /**
     * @inheritdoc
     * @param string $strPath (=> $key)
     */
    protected function addValueEngine($strPath, $value)
    {
        // Return result
        return ToolBoxPathTableData::setValue(
            $this->getUseTabDataSrc(),
            $strPath,
            $value,
            $this->getStrConfigPathSeparator(),
            true
        );
    }



    /**
     * @inheritdoc
     * @param string $strPath (=> $key)
     */
    protected function removeValueEngine($strPath)
    {
        // Return result
        return ToolBoxPathTableData::removeValue(
            $this->getUseTabDataSrc(),
            $strPath,
            $this->getStrConfigPathSeparator()
        );
    }
	
	
	
}