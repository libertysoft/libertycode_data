<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\data\data\table\path\exception;

use liberty_code\data\data\table\path\library\ConstPathTableData;



class PathInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $path
     */
	public function __construct($path)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstPathTableData::EXCEPT_MSG_PATH_INVALID_FORMAT, strval($path));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified path has valid format.
	 * 
     * @param mixed $path
	 * @param string $strPathSeparator = ConstPathTableData::DATA_DEFAULT_VALUE_PATH_SEPARATOR
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($path, $strPathSeparator = ConstPathTableData::CONFIG_DEFAULT_PATH_SEPARATOR)
    {
		// Init var
        $strPathSeparator = (
            (is_string($strPathSeparator) && (trim($strPathSeparator) != '')) ?
                $strPathSeparator :
                ConstPathTableData::CONFIG_DEFAULT_PATH_SEPARATOR
        );
		$result =
            // Check valid string
            is_string($path) && (trim($path) != '');
		
		// Check valid string
		if($result)
		{
			// Run all keys in path
			$tabPath = explode($strPathSeparator, $path);
			for($intCpt = 0; ($intCpt < count($tabPath)) && $result; $intCpt++)
			{
				// Check key not empty
				$result = (trim($tabPath[$intCpt]) != '');
			}
		}
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($path);
		}
		
		// Return result
		return $result;
    }
	
	
	
}