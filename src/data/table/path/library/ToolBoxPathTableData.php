<?php
/**
 * Description :
 * Following features manage specified array, with path selection.
 *
 * Example where '/' considered as path separator:
 * Path: '/key1/key2/.../keyN' => array[key1][key2]...[keyN]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\data\data\table\path\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\table\library\ToolBoxTable;



class ToolBoxPathTableData extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get value,
     * from specified data array,
     * and specified path.
	 * 
	 * @param array &$tabData
	 * @param string $strPath
	 * @param string $strPathSeparator
	 * @return null|mixed
	 */
	public static function getValue(
	    array &$tabData,
        $strPath,
        $strPathSeparator
    )
	{
        // Init var
        $tabKey =  (
            (
                is_string($strPath) &&
                (trim($strPath) != '') &&
                is_string($strPathSeparator) &&
                (trim($strPathSeparator) != '')

            ) ?
                explode($strPathSeparator, $strPath):
                null
        );
        $result = (
            (!is_null($tabKey)) ?
                ToolBoxTable::getItem($tabData, $tabKey, null) :
                null
        );

        // Return result
        return $result;
	}
	
	
	
	
	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * Set specified value,
     * on specified data array,
     * from specified path.
	 * 
	 * @param array &$tabData
	 * @param string $strPath
	 * @param mixed $value
	 * @param boolean $boolCreate = false
	 * @param string $strPathSeparator
	 * @return boolean
	 */
	public static function setValue(
	    array &$tabData,
        $strPath,
        $value,
        $strPathSeparator,
        $boolCreate = false
    )
	{
        // Init var
        $tabKey =  (
            (
                is_string($strPath) &&
                (trim($strPath) != '') &&
                is_string($strPathSeparator) &&
                (trim($strPathSeparator) != '')

            ) ?
                explode($strPathSeparator, $strPath):
                null
        );
        $result = (
            ((!is_null($tabKey)) && (!is_null($value))) ?
                ToolBoxTable::setItem($tabData, $tabKey, $value, $boolCreate) :
                false
        );

        // Return result
        return $result;
	}
	
	
	
	/**
	 * Remove value,
     * on specified data array,
     * from specified path.
	 * 
	 * @param array &$tabData
	 * @param string $strPath
	 * @param string $strPathSeparator
	 * @return boolean
	 */
	public static function removeValue(
	    array &$tabData,
        $strPath,
        $strPathSeparator
    )
	{
        // Init var
        $tabKey =  (
            (
                is_string($strPath) &&
                (trim($strPath) != '') &&
                is_string($strPathSeparator) &&
                (trim($strPathSeparator) != '')

            ) ?
                explode($strPathSeparator, $strPath):
                null
        );
        $result = (
            (!is_null($tabKey)) ?
                ToolBoxTable::removeItem($tabData, $tabKey) :
                false
        );

        // Return result
        return $result;
	}
	
	
	
}