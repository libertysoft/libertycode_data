<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\data\data\table\path\library;



class ConstPathTableData
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_PATH_SEPARATOR = 'path_separator';

	// Default configuration
	const CONFIG_DEFAULT_PATH_SEPARATOR = '/';



	// Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the path table data configuration standard.';
	const EXCEPT_MSG_PATH_INVALID_FORMAT =
        'Following path "%1$s" invalid! 
        The path must be a string, not empty and use path separator as key separator. Each key must be a string not empty.';
}