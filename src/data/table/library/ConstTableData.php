<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\data\data\table\library;



class ConstTableData
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	// Exception message constants
    const EXCEPT_MSG_DATA_SRC_INVALID_FORMAT = 'Following data source "%1$s" invalid! The data source must be a valid array.';
    const EXCEPT_MSG_KEY_INVALID_FORMAT =
        'Following key "%1$s" invalid! 
        The key must be a string, not empty or an integer.';
    const EXCEPT_MSG_VALUE_INVALID_FORMAT =
        'Following value "%1$s" invalid! 
        The value can\'t be null.';
}