<?php
/**
 * Description :
 * This class allows to define table data.
 * Table data uses array as source like:
 * array['key'] = value
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\data\data\table\model;

use liberty_code\data\data\handle\model\HandleData;

use liberty_code\data\data\library\ConstData;
use liberty_code\data\data\table\exception\DataSrcInvalidFormatException;
use liberty_code\data\data\table\exception\KeyInvalidFormatException;
use liberty_code\data\data\table\exception\ValueInvalidFormatException;



/**
 * @method array getDataSrc() @inheritdoc
 * @method void setDataSrc(array $dataSrc) @inheritdoc
 */
class TableData extends HandleData
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstData::DATA_KEY_DEFAULT_DATA_SRC))
        {
            $this->__beanTabData[ConstData::DATA_KEY_DEFAULT_DATA_SRC] = array();
        }
    }





	// Methods validation
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstData::DATA_KEY_DEFAULT_DATA_SRC:
                    DataSrcInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





	// Methods getters
	// ******************************************************************************

    /**
     * Get reference of array of values,
     * to use on features.
     *
     * @return array
     */
    protected function &getUseTabDataSrc()
    {
        // Use property array, as array of item
        return $this->__beanTabData[ConstData::DATA_KEY_DEFAULT_DATA_SRC];
    }



    /**
     * @inheritdoc
     */
    public function getTabKey()
    {
        // Init var
        $tabDataSrc = $this->getUseTabDataSrc();
        $result = array_keys($tabDataSrc);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @param string|integer $key
     */
    protected function getValueEngine($key)
    {
        // Init var
        $result = null;
        $tabDataSrc = $this->getUseTabDataSrc();

        // Get value, if found
        if(array_key_exists($key, $tabDataSrc))
        {
            $result = $tabDataSrc[$key];
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @param string|integer $key
     * @throws KeyInvalidFormatException
     */
    public function getValue($key)
    {
        // Check path format
        KeyInvalidFormatException::setCheck($key);

        // Return result
        return parent::getValue($key); // Call parent method
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param string|integer $key
     */
    protected function setValueEngine($key, $value)
    {
        // Init var
        $tabDataSrc = &$this->getUseTabDataSrc();

        // Set value
        $tabDataSrc[$key] = $value;

        // Return result
        return true;
    }



    /**
     * @inheritdoc
     * @param string|integer $key
     * @throws KeyInvalidFormatException
     * @throws ValueInvalidFormatException
     */
    public function setValue($key, $value)
    {
        // Check key, value format
        ValueInvalidFormatException::setCheck($value);

        // Return result
        return parent::setValue($key, $value); // Call parent method
    }



    /**
     * @inheritdoc
     * @param string|integer $key
     */
    protected function addValueEngine($key, $value)
    {
        // Return result
        return $this->setValueEngine($key, $value);
    }



    /**
     * @inheritdoc
     * @param string|integer $key
     * @throws KeyInvalidFormatException
     * @throws ValueInvalidFormatException
     */
    public function addValue($key, $value)
    {
        // Check key, value format
        ValueInvalidFormatException::setCheck($value);

        // Return result
        return parent::addValue($key, $value); // Call parent method
    }



    /**
     * @inheritdoc
     * @param string|integer $key
     */
    protected function removeValueEngine($key)
    {
        // Init var
        $tabDataSrc = &$this->getUseTabDataSrc();

        // Remove value
        unset($tabDataSrc[$key]);

        // Return result
        return true;
    }



}