LibertyCode_Data
================



Description
-----------

Library contains data components, 
allows to manage key-value pairs as dictionary, 
on specified data source (stored on memory support).

_Usage_

- Data differences from bean collection

    -   Interface requirement.
    -   Less key name restriction.

- Data differences from register

    -   Handle storage support on memory, 
        to use specified data source, 
        with any format.

- Usage advise

    -   Dictionary, 
        with interface requirement, 
        on memory support, with specified data source, in any format.
    -   Simple data storage: example: 
        - Configuration, previously parsed from string source, file content, etc...
        - Parameters
        - Etc...
    -   Data source update tasks: example:
        - Add, set or delete some sub-data on data source.
        - Get updated data source to re-use in other place.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/data ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/data": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Data

Data allows to design basic data class, 
with structure to manage key-value pairs (get, add, update, remove),
from data source.

_Elements_

- Data
    
    Allows to design basic data, 
    can be used as base of all data type.
    
- HandleData

    Extends data features. 
    Allows to handle data and action control.
    
- TableData
    Extends handle data features. 
    Uses array as data source.
    
- PathTableData

    Extends table data features. 
    Uses keys as paths (ex: /key1/key2/.../keyN),
    to manage key-value pairs, 
    on array data source.

_Example_

```php
use liberty_code\data\data\table\path\model\PathTableData;
$data = new PathTableData();
...
// Set array data source
$data->setDataSrc(array(...));
...
// Register specified value for specfied path
$data->putValue('/key1/key2/.../keyN', '...');
...
// Show specified value
var_dump($data->getValue('/key1/key2/.../keyN'));
...
```

---


