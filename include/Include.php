<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/data/library/ConstData.php');
include($strRootPath . '/src/data/exception/KeyFoundException.php');
include($strRootPath . '/src/data/exception/KeyNotFoundException.php');
include($strRootPath . '/src/data/api/DataInterface.php');
include($strRootPath . '/src/data/model/DefaultData.php');

include($strRootPath . '/src/data/handle/library/ConstHandleData.php');
include($strRootPath . '/src/data/handle/exception/HandleKeyInvalidFormatException.php');
include($strRootPath . '/src/data/handle/exception/HandleValueInvalidFormatException.php');
include($strRootPath . '/src/data/handle/exception/HandleKeyNotRemoveException.php');
include($strRootPath . '/src/data/handle/model/HandleData.php');

include($strRootPath . '/src/data/table/library/ConstTableData.php');
include($strRootPath . '/src/data/table/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/data/table/exception/KeyInvalidFormatException.php');
include($strRootPath . '/src/data/table/exception/ValueInvalidFormatException.php');
include($strRootPath . '/src/data/table/model/TableData.php');

include($strRootPath . '/src/data/table/path/library/ConstPathTableData.php');
include($strRootPath . '/src/data/table/path/library/ToolBoxPathTableData.php');
include($strRootPath . '/src/data/table/path/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/data/table/path/exception/PathInvalidFormatException.php');
include($strRootPath . '/src/data/table/path/model/PathTableData.php');